--1) 2 veya daha fazla defa kiralanan araçlardan 3 tanesinin marka, plaka ve kaç kez kiralandiklarini bulunuz ?
 
select top(3) COUNT(K.Plaka) KiralanmaSayısı, A.plaka, A.Marka
from Arac A,Kiralama K
where A.Plaka=K.Plaka
group by A.plaka, A.Marka
having COUNT(K.Plaka) >= 2

					
--2) 04-AP-2000 plakali arac en son hangi subeye birakilmistir ?
 
select top(1) K.Plaka, S.Adi, K.IadeTarihi
from KiralamaYapma KY, Kiralama K, Sube S
where KY.KiraId=K.KiraId and K.SubeKodu=S.SubeKodu	and K.Plaka='04-AP-2000'
order by K.IadeTarihi desc
  
--3) Sigorta sirketlerine göre arac sayilarini bulunuz ?

select S.Adi, COUNT(A.Plaka) AracSayısı
from SigortaSirketi S, Arac A, Sigortalama Si
where Si.Plaka=A.Plaka and S.SirketNo=Si.SirketNo
group by S.Adi
 
--4) Ümit Durus adli calisan hangi musterilerle ilgilenmistir ?

select M.Ad, M.Soyad
from Kiralama K
inner join Musteri M on M.VatandaslikNo=K.VatandaslikNo
inner join Calisan C on C.TC=K.TC	
where C.Adi='Ümit' and C.Soyadi='Durus' 		
 
--5) Subelerden kiralama yapan yabanci uyruklu musteriler ?

select M.Ad, M.Soyad
from Musteri M, Uyruk U, Kiralama K
where M.ID=U.ID and M.VatandaslikNo=K.VatandaslikNo 
except
select M2.Ad, M2.Soyad
from Musteri M2, Uyruk U2, Kiralama K2
where M2.ID=U2.ID and M2.VatandaslikNo=K2.VatandaslikNo and U2.Vatandaslik='Türk'

--6) En fazla arac kiralanan 5 sehir ve subeyi bulunuz ?
select top (5) S.Adi, Su.Adi, COUNT(K.Plaka) KiralamaSayısı
from Sehir S, Sube Su, Kiralama K
where S.PlakaKodu = Su.PlakaKodu and Su.SubeKodu = K.SubeKodu
group by S.Adi,Su.Adi
order by COUNT(K.Plaka) desc

--7) Kiraya verilmeyen araçlari bulunuz ?					
select *
from Arac A	
where A.Plaka not in(	
						select A2.Plaka
						from Arac A2, Kiralama K2
						where K2.Plaka=A2.Plaka
					)

--8) 01-01-2014 ile 07-25-2016 tarihleri arasinda hangi sube ne kadar kazanmistir ?

select S.Adi, sum(F.Tutari) Kazanc
from Kiralama K
inner join Sube S on K.SubeKodu=S.SubeKodu
inner join Fatura F on K.FaturaNo=F.FaturaNo
where K.AlisTarihi>'01-01-2014' and K.IadeTarihi<'07-25-2016'
group by S.Adi 

--9) Rent a car sirketinin ilk calisani kimdir ?

select C.Adi, C.Soyadi, C.Departman, C.Maas
from Calisan C, Calistirma Ca 
where C.TC = Ca.TC and Ca.IseBaslamaTarihi in( 
												select MIN(Ca.IseBaslamaTarihi)
												from Calistirma Ca
											  )
	
--10) Hem 41-AF-2000  hem de 41-AD-1587 plakali araci kiralayan kisiyi bulunuz ?

select M.Ad, M.Soyad, M.Ehliyet
from Musteri M, Kiralama K, Arac A
where K.VatandaslikNo = M.VatandaslikNo and K.Plaka = A.Plaka
and A.Plaka = '41-AF-2000'
intersect
select M.Ad, M.Soyad, M.Ehliyet
from Musteri M, Kiralama K, Arac A
where K.VatandaslikNo = M.VatandaslikNo and K.Plaka = A.Plaka
and A.Plaka = '41-AD-1587' 

