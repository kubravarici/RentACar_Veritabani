create table Sehir	(
						Adi varchar(20),
						PlakaKodu int,
						primary key (PlakaKodu)
					);
					
					
create table Sube	(
						Adi varchar(20),
						TelNo varchar(20) unique,
						SubeKodu int ,
						PlakaKodu int,
						primary key (SubeKodu),
						foreign key (PlakaKodu) references Sehir (PlakaKodu)
					);
					

create table Calisan	(
							Adi varchar(20),
							Soyadi varchar(20),
							Mail varchar(30) unique,
							Telefon varchar(20) unique,
							Departman varchar(30),
							Maas int,
							TC varchar(20),
							SubeKodu int,
							primary key (TC),
							foreign key (SubeKodu) references Sube(SubeKodu)	
						);
						
						
create table Calistirma		(
								IseBaslamaTarihi date,
								SubeKodu int,
								TC varchar(20),
								foreign key (SubeKodu) references Sube (SubeKodu),
								foreign key (TC) references Calisan (TC)								
							);
							
							
create table Arac	(
						Marka varchar(20),
						Renk varchar(20),
						AracTuru varchar(20),
						YakitTuru varchar(20),
						Plaka varchar(20),
						SubeKodu int,
						primary key (Plaka),
						foreign key (SubeKodu) references Sube(SubeKodu)
					);
					
					
create table SigortaSirketi	(
								Adi varchar(20),
								SirketNo int,
								primary key (SirketNo)
							);
							
							
							
create table Sigortalama	(
								SigortaSuresi varchar(20),
								Plaka varchar(20),
								SirketNo int,
								foreign key (Plaka) references Arac(Plaka),
								foreign key (SirketNo) references SigortaSirketi(SirketNo)
							);
							
							
							
create table Uyruk	(
						Vatandaslik varchar(20) not null,
						ID int,
						primary key (ID)
					);
					
					
					
create table Musteri	(
							Ad varchar(20),
							Soyad varchar(20),
							Adres varchar(50),
							Telefon varchar(20) unique,
							Mail varchar(20) unique,
							Ehliyet varchar(20),
							VatandaslikNo int,
							ID int,
							primary key (VatandaslikNo),
							foreign key (ID) references Uyruk (ID)
						);
					
						
create table Odeme	(
						OdemeTuru varchar(20),							
						ID int,
						primary key (ID)
					);
					
										
					
					
create table Fatura		(
							Tutari int,
							KesimTarihi date,
							FaturaNo int,
							ID int,
							primary key (FaturaNo),					
							foreign key (ID) references Odeme(ID)
						);	
						
						
						
						
create table Kiralama	(
							KiraId int,
							AlisTarihi date,
							IadeTarihi date,
							Plaka varchar(20),
							SubeKodu int,
							TC varchar(20),
							VatandaslikNo int,
							FaturaNo int,
							primary key (KiraId),
							foreign key (Plaka) references Arac(Plaka),
							foreign key (SubeKodu) references Sube(SubeKodu),
							foreign key (TC) references Calisan(TC),
							foreign key (VatandaslikNo) references Musteri(VatandaslikNo),
							foreign key (FaturaNo) references Fatura(FaturaNo)						
						);
											

create table KiralamaYapma	(
								IadeYeri int not null,
								SubeKodu int,
								KiraId int,
								foreign key (SubeKodu) references Sube (SubeKodu),
								foreign key (KiraId) references Kiralama(KiraId),
								primary key (KiraID)
							);	
							
																											