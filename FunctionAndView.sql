--1)Girilen calisan�n hangi musterilerle ilgilendi�ini veren fonksiyonu yaz�n�z?

create function MusteriIlgilenme(@CalisnAd varchar(20),@CalisanSoyad varchar(20))
returns table
as
return(	select C.Adi CalisanAdi, C.Soyadi CalisanSoyadi, M.Ad MusteriAdi, M.Soyad MusteriSoyadi
			from Kiralama K
			inner join Musteri M on M.VatandaslikNo=K.VatandaslikNo
			inner join Calisan C on C.TC=K.TC
			where C.Adi=@CalisnAd and C.Soyadi=@CalisanSoyad
			group by C.Adi,C.Soyadi,M.Ad, M.Soyad
		)	

select * from MusteriIlgilenme('�mit','Durus')


--2)Girilen �ehire g�re subelerin arac kiralama say�lar�n� bulan fonksiyon ?

create function KiralamaSayisi(@sehir varchar(10))
returns @Kiralama table(SehirAdi varchar(10),SubeAdi varchar(10),KiralamaSay�s� int)
as begin
insert into @Kiralama
select S.Adi, Su.Adi, COUNT(K.Plaka) KiralamaSay�s�
from Sehir S, Sube Su, Kiralama K
where S.PlakaKodu = Su.PlakaKodu and Su.SubeKodu = K.SubeKodu and S.Adi=@sehir
group by S.Adi,Su.Adi
return
end

select * from KiralamaSayisi('�stanbul')


--3)Rent a car �irketinin girilen tarihler arasindaki toplam kazanc�n� veren fonksiyon?

create function ToplamKazanc(@Alis varchar(10),@Varis varchar(10))
returns int
as
begin 
declare @sonuc int;
            select @sonuc= sum(F.Tutari) 
			from Kiralama K
			inner join Sube S on K.SubeKodu=S.SubeKodu
			inner join Fatura F on K.FaturaNo=F.FaturaNo
			where K.AlisTarihi>@Alis and K.IadeTarihi<@Varis
return @sonuc
end

print dbo.ToplamKazanc('01-01-2014','09-01-2015') 

--4)Plakas� girilen arac�n en son hangi subeye b�rak�ld���n� veren fonksiyon ?
 
create function SonSube(@Plaka varchar(20))
returns varchar(20)
as
begin
return(	select top(1) KY.IadeYeri 
		from KiralamaYapma KY, Kiralama K, Sube S
		where KY.KiraId=K.KiraId and K.SubeKodu=S.SubeKodu and K.Plaka=@Plaka 
		order by K.IadeTarihi desc
	  )
end

select dbo.SonSube('41-AD-1587') �adeYeri


--viewler--

--1)Yabanc� uyruklu m��terileri getiren view ?

create view YabanciMusteriler as 
select M.Ad, M.Soyad, U.Vatandaslik, K.KiraID
from Musteri M inner join Uyruk U on M.ID = U.ID inner join Kiralama K on K.VatandaslikNo = M.VatandaslikNo
except
select M.Ad,M.Soyad, U.Vatandaslik, K.KiraID
from Musteri M inner join Uyruk U on M.ID = U.ID inner join Kiralama K on K.VatandaslikNo = M.VatandaslikNo
where U.Vatandaslik = 'T�rk'

select *
from YabanciMusteriler


--2)Yabanc� uyruklu m��terilerin yapt��� kiralamalar�n s�relerine g�re derecelendirilmesini sa�layan view?

Create view Derecelendirme as
select YM.Ad, YM.Soyad,YM.Vatandaslik, DATEDIFF(day, K.AlisTarihi, K.IadeTarihi) KiraGunSayisi,
case 
	when DATEDIFF(day, K.AlisTarihi, K.IadeTarihi) <=5 then 'K�sa S�reli'
	when DATEDIFF(day, K.AlisTarihi, K.IadeTarihi) >5 and DATEDIFF(day, K.AlisTarihi, K.IadeTarihi) <=10 then 'Orta S�reli'
	else 'Uzun S�reli'
end as KiralamaSuresi
from dbo.YabanciMusteriler YM inner join Kiralama K on YM.KiraID = K.KiraID

select * from Derecelendirme


--3)Kiralama yapan elemanlar�n �irkete kazand�rd��� toplam tutar� getiren view ?

create view KiralamaYapanlar as
select  C.Telefon ,C.Adi, C.Soyadi, sum(F.Tutari) Kazan�
from Calisan C inner join Kiralama K on C.TC = K.TC inner join Fatura F on K.FaturaNo = F.FaturaNo
group by C.Telefon ,C.Adi, C.Soyadi

select *
from KiralamaYapanlar 
order by Kazan� desc

--4)rent a car �irketinde hangi arabadan ka� tane bulundu�unu g�steren view?
create view KacSube as
select A.Marka, count(S.Adi) KacTane
from Arac A inner join Sube S on A.SubeKodu=S.SubeKodu
group by A.Marka

select *
from KacSube







